<?php

declare(strict_types=1);

namespace App\Tests\Product\UI\Web;

use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ProductControllerTest extends WebTestCase
{
    /**
     * @test
     */
    public function shouldCreate(): void
    {
        $faker = Factory::create();
        $name = $faker->sentence;
        $price = $faker->numberBetween(1, 10000);

        $response = $this->post('/products', ['name' => $name, 'priceAmount' => $price]);
        $responseBody = json_decode($response->getContent(), true);

        $this->assertEquals(200, $response->getStatusCode());
        static::assertEquals($name, $responseBody['name']);
        static::assertEquals($price, $responseBody['price']['amount']);
        static::assertEquals('EUR', $responseBody['price']['currency']);
    }

    private function post(string $uri, array $body): Response
    {
        $client = self::createClient();
        $client->followRedirects();
        $client->request(
            'POST',
            $uri,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($body)
        );

        return $client->getResponse();
    }

    /**
     * @test
     */
    public function shouldValidateTooShortName(): void
    {
        $name = 'a';
        $price = 11;

        $response = $this->post('/products', ['name' => $name, 'priceAmount' => $price]);
        $responseBody = json_decode($response->getContent(), true);

        $this->assertEquals(400, $response->getStatusCode());
        static::assertEquals('[name]', $responseBody[0]['field']);
        static::assertEquals($name, $responseBody[0]['context']);
    }
}
