<?php

declare(strict_types=1);

namespace App\Product\ReadModel;

use App\Shared\ResourceNotFoundException;
use Exception;

class ProductNotFoundException extends Exception implements ResourceNotFoundException
{

}