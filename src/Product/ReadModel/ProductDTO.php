<?php

declare(strict_types=1);

namespace App\Product\ReadModel;

use Money\Money;

class ProductDTO
{
    private int $id;
    private string $name;
    private Money $price;

    public function __construct(int $id, string $name, Money $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }

    public static function fromStorage(array $data)
    {
        return new self((int)$data['id'], $data['name'], $data['price']);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): Money
    {
        return $this->price;
    }
}
