<?php

declare(strict_types=1);

namespace App\Product\Application\Command;

class CreateProduct
{
    private string $name;
    private int $priceAmount;

    public function __construct(string $name, int $priceAmount)
    {
        $this->name = $name;
        $this->priceAmount = $priceAmount;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPriceAmount(): int
    {
        return $this->priceAmount;
    }
}
