<?php

declare(strict_types=1);

namespace App\Product\Application;

use App\Product\Application\Command\CreateProduct;
use App\Product\DomainModel\ProductCollectionFactory;

class ProductService
{
    private ProductCollectionFactory $collectionFactory;

    public function __construct(ProductCollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    public function create(CreateProduct $command): int
    {
        return $this->collectionFactory
            ->createCollection()
            ->addProduct($command->getName(), $command->getPriceAmount());
    }
}
