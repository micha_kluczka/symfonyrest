<?php

declare(strict_types=1);

namespace App\Product\DomainModel;

class ProductCollectionFactory
{
    private ProductRepository $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function createCollection(): ProductCollection
    {
        return new ProductCollection($this->repository);
    }
}