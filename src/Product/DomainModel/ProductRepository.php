<?php

declare(strict_types=1);

namespace App\Product\DomainModel;

use Money\Money;

interface ProductRepository
{
    public function addProduct(string $name, Money $price): int;
}