<?php

declare(strict_types=1);

namespace App\Product\UI\Web;

use App\Product\Application\ProductService;
use App\Product\ReadModel\ProductNotFoundException;
use App\Product\ReadModel\ProductReadModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends AbstractController
{
    public function create(Request $request, ProductService $service): Response
    {
        $command = CommandFactory::buildCreateProduct($request);
        $productId = $service->create($command);

        return $this->redirectToRoute('product:getDetails', ['productId' => $productId]);
    }

    /**
     * @throws ProductNotFoundException
     */
    public function getDetails(int $productId, ProductReadModel $readModel): Response
    {
        $product = $readModel->getProduct($productId);

        return $this->json(new ProductPresenter($product));
    }
}