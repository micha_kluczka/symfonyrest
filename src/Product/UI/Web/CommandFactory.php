<?php

declare(strict_types=1);

namespace App\Product\UI\Web;

use App\Product\Application\Command\CreateProduct;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CommandFactory
{
    public static function buildCreateProduct(Request $request): CreateProduct
    {
        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);

        return $serializer->deserialize($request->getContent(), CreateProduct::class, 'json');
    }
}