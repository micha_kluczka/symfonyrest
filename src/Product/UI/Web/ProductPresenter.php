<?php

declare(strict_types=1);

namespace App\Product\UI\Web;

use App\Product\ReadModel\ProductDTO;
use JsonSerializable;

class ProductPresenter implements JsonSerializable
{
    private ProductDTO $productDTO;

    public function __construct(ProductDTO $productDTO)
    {
        $this->productDTO = $productDTO;
    }


    public function jsonSerialize(): array
    {
        return [
            'id' => $this->productDTO->getId(),
            'name' => $this->productDTO->getName(),
            'price' => $this->productDTO->getPrice()
        ];
    }
}