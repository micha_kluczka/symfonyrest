<?php

declare(strict_types=1);

namespace App\Product\UI\Web;

use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\PositiveOrZero;
use Symfony\Component\Validator\Constraints\Type;

final class ValidationRules
{
    public const CREATE = [
        'name' => [
            [Type::class, 'string'],
            NotBlank::class,
            [Length::class, ['min' => 3, 'max' => 255]]
        ],
        'priceAmount' => [
            [Type::class, 'integer'],
            NotBlank::class,
            PositiveOrZero::class
        ],
    ];
}