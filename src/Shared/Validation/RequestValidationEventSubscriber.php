<?php

declare(strict_types=1);

namespace App\Shared\Validation;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Validator\ConstraintViolationInterface;

class RequestValidationEventSubscriber implements EventSubscriberInterface
{
    private RequestValidator $validator;

    public function __construct(RequestValidator $validator)
    {
        $this->validator = $validator;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest',
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $validationRules = $event->getRequest()->get('validationRules');

        if (!empty($validationRules)) {
            $errors = $this->validator->validate($event->getRequest()->getContent(), constant($validationRules));
            if (count($errors) > 0) {
                $body = [];

                /** @var ConstraintViolationInterface $error */
                foreach ($errors as $error) {
                    $body[] = [
                        'field' => $error->getPropertyPath(),
                        'context' => $error->getInvalidValue(),
                        'message' => $error->getMessage(),
                        'code' => $error->getCode(),
                    ];
                }

                $response = new JsonResponse($body, 400);
                $event->setResponse($response);
                return;
            }
        }
    }
}