<?php

declare(strict_types=1);

namespace App\Shared\Validation;

use Exception;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends Exception
{
    private ConstraintViolationListInterface $errors;

    public static function create(ConstraintViolationListInterface $errors): self
    {
        $self = new self();
        $self->errors = $errors;

        return $self;
    }

    public function getErrors(): ConstraintViolationListInterface
    {
        return $this->errors;
    }
}