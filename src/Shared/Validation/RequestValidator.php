<?php

declare(strict_types=1);

namespace App\Shared\Validation;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestValidator
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validate(string $requestContent, array $rules): ConstraintViolationListInterface
    {
        $serializer = new Serializer([], [new JsonEncoder()]);

        $payload = $serializer->decode($requestContent, 'json');
        return $this->validator->validate($payload, $this->parseRules($rules));
    }

    private function parseRules(array $rawRules): Collection
    {
        $rules = [];

        foreach ($rawRules as $field => $rawFieldRules) {
            $fieldRules = [];
            foreach ($rawFieldRules as $rule) {
                if (is_array($rule)) {
                    $fieldRules[] = new $rule[0]($rule[1]);
                } else {
                    $fieldRules[] = new $rule();
                }
            }
            $rules[$field] = new Optional($fieldRules);
        }

        return new Collection($rules);
    }
}